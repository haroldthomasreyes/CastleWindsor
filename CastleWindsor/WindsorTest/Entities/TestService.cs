﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WindsorTest.Entities
{
    public interface ITestService
    {
        string GetName();
    }

    public class TestService : ITestService
    {
        public string GetName()
        {
            return "Test Service";
        }
    }
}